class CreateTestCardDetails < ActiveRecord::Migration
  def change
    create_table :test_card_details do |t|
      t.string :name
      t.string :number
      t.string :cvv
      t.string :exp_month
      t.string :exp_year
      t.string :person_id

      t.timestamps
    end
  end
end
