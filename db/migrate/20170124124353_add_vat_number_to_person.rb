class AddVatNumberToPerson < ActiveRecord::Migration
  def change
    add_column :people, :vat_number, :string
    add_column :people, :vat_country_code, :string
  end
end
