class AddDiscountPercentToTransactions < ActiveRecord::Migration
  def change
    add_column :transactions, :discount_percent, :integer, :default => 0
  end
end
