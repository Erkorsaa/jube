class OwnerReceiptPdf < Prawn::Document
  include MoneyRails::ActionViewExtension
  def initialize(transaction, fee, price, receipt_date, view)
    super(top_margin: 70)
    @transaction = transaction
    @fee = fee
    @price = price
    @receipt_date = receipt_date
    if @transaction.discount_percent > 0
      @discount = (@price * 0.01 * @transaction.discount_percent).round(2)
      @total_price = @price - @discount
    else
      @total_price = @price - @fee
    end
    @vat_price = (@total_price * 0.2).round(2)
    logo
    table([["Hallo there! This receipt is sent from JUBE to both parties as a service. - This transaction is made between the Renter and Owner. - The secure online payment is handled in collaboration with Stripe. <b>www.Stripe.com</b> - As an owner the payment usually takes 1-3 business days for to reach your bank account. But in some cases it takes up to 7 business days.- Do you have questions? Reach out to us."]],
      :position => :center,
      :width => 300,
      :cell_style => {:size => 7,
                      :border_width => 0,
                      :padding => [10, 0, 10, 0],
                      :inline_format => true,
                      :text_color => "727e8d"})
    owner_renter_details
    receipt_details
    price_details
    move_down 20
    footer
  end

  def logo
    logopath =  "#{Rails.root}/app/assets/images/m_img0.png"
    table([[{image: logopath, position: :left, :image_height => 50, :image_width => 50}, "<b><color rgb='425065'>RECEIPT</color></b>"],], 
      :position => :center,
      :width => 300,
      :cell_style => {:border_width => 0, 
                      :valign => :center, 
                      :align => :right,
                      :borders => [:bottom],
                      :border_width => 1,
                      :border_color => "dde5f1",
                      :inline_format => true,
                      :size => 10
                      })
  end

  def owner_renter_details
    renter_img = "#{Rails.root}/app/assets/images/m_img1.png"
    owner_img = "#{Rails.root}/app/assets/images/m_img2.png"
    renter = make_table([["Renter Information:"], ["<b>#{@transaction.starter.full_name.capitalize}</b>"]],:cell_style => {:border_width => 0, :inline_format => true, :size => 7, :padding => [1, 0, 1, 0], :text_color => "425065"})
    owner = make_table([[" Owner Information:"], ["<b>#{@transaction.author.full_name.capitalize}</b>"]],:cell_style => {:border_width => 0, :inline_format => true, :size => 7, :padding => [1, 0, 1, 0], :text_color => "425065"})
    renter_info = make_table([
                    [make_table([[{image: renter_img, image_width: 10, image_height: 10}, renter]],:cell_style => {:border_width => 0, :inline_format => true})],
                    [(@transaction.starter.exact_address.present? ? @transaction.starter.exact_address : @transaction.starter.street_address)],
                    ["Phone: <b>#{@transaction.starter.phone_number}</b> -  VAT/CVR: <b>#{@transaction.starter.vat_number.present? ? @transaction.starter.vat_number : 'Not specified'}</b>"]
                  ],:cell_style => {:border_width => 0, :inline_format => true, :size => 7, :text_color => "727e8d"}, :width => 150)
    owner_info = make_table([
                  [make_table([[{image: owner_img, image_width: 10, image_height: 10}, owner]],:cell_style => {:border_width => 0, :inline_format => true})],
                  [(@transaction.author.exact_address.present? ? @transaction.author.exact_address : @transaction.author.street_address)],
                  ["Phone: <b>#{@transaction.author.phone_number}</b> -  VAT/CVR: <b>#{@transaction.author.vat_number.present? ? @transaction.author.vat_number : 'Not specified'}</b>"]
                ],:cell_style => {:border_width => 0, :inline_format => true, :size => 7, :text_color => "727e8d"}, :width => 150)
    table([[renter_info, owner_info]],:cell_style => {:border_width => 1, :border_color => "dde5f1", :padding => [10, 0, 10, 0]}, :position => :center )
  end

  def receipt_details
    receipt_no_img = "#{Rails.root}/app/assets/images/m_img3.png"
    receipt_date_img = "#{Rails.root}/app/assets/images/m_img4.png"
    receipt_total_img = "#{Rails.root}/app/assets/images/m_img5.png"
    receipt_no = make_table([["Receipt No"], ["<b>#{@transaction.receipt_number}</b>"]],:cell_style => {:border_width => 0, :inline_format => true, :size => 7, :padding => [1, 0, 1, 0]})
    receipt_date = make_table([["Receipt Date"], ["<b>#{@receipt_date}</b>"]],:cell_style => {:border_width => 0, :inline_format => true, :size => 7, :padding => [1, 0, 1, 0]})
    receipt_total = make_table([[" Receipt Total"], ["<b>#{@total_price}</b>"]],:cell_style => {:border_width => 0, :inline_format => true, :size => 7, :padding => [1, 0, 1, 0]})
    table([[
      make_table([[{image: receipt_no_img, image_width: 10, image_height: 10}, receipt_no]],:cell_style => {:border_width => 0}, :width => 100),
      make_table([[{image: receipt_date_img, image_width: 10, image_height: 10}, receipt_date]],:cell_style => {:border_width => 0}, :width => 100),
      make_table([[{image: receipt_total_img, image_width: 10, image_height: 10}, receipt_total]],:cell_style => {:border_width => 0}, :width => 100)
      ]],:cell_style => {:border_width => 1, :border_color => "dde5f1", :text_color => "425065", :padding => [10, 0, 10, 0]}, :position => :center)
  end

  def price_details
    currency = @transaction.unit_price_currency
    prices = [
      ["<b><color rgb='FFFFFF'>Description</color></b>", "<b><color rgb='FFFFFF'>Fee</color></b>", "<b><color rgb='FFFFFF'>Cost</color></b>", "<b><color rgb='FFFFFF'>Total</color></b>"],
      ["<b>#{@transaction.listing.title.capitalize}</b>", "", "<color rgb='67bffd'>#{currency} #{@price}</color>", ""],
      ["<b>VAT of Total Price</b>", "", "<color rgb='67bffd'>#{currency} #{@vat_price}</color>", ""],
      ["<b>Total Price Transferred</b>", "", "", "<b>#{currency} #{@total_price}</b>"]
      ]
    if @transaction.discount_percent > 0
      prices.insert(2, ["<b>Discount(#{@transaction.discount_percent}%)</b>", "", "<color rgb='67bffd'>-#{currency} #{@discount}</color>", ""])
    else
      prices.insert(2, ["<b>Application Fee</b>", "<color rgb='67bffd'>#{currency} #{@fee}</color>", "", ""])
    end
    table(prices, :position => :center, :cell_style => {:align => :center, :inline_format => true, :border_width => 1, :border_color => "dde5f1", :padding => 15, :size => 8}, 
      :row_colors => ["67bffd", "eff3f7", "eff3f7", "eff3f7", "eff3f7"], :width => 300)
  end

  def footer
    table([
      ["All prices include Moms / VAT "]
      ],:width => 300, :cell_style => {:align => :center, :inline_format => true, :border_width => 0, :size => 7, :text_color => "425065"}, :position => :center)
    table([
      ["<b>Website:</b> www.jube.co "],
      ["<b>Address:</b> Bregnerødgade 18, 1 - 2200 Kbh. N. "],
      ["<b>Contact information:</b>  +45 20517216 // +45 53841953 "],
      [" Jube IVS: 38350420 "]
      ],:width => 300, :cell_style => {:align => :center, :inline_format => true, :border_width => 0, :size => 7, :padding => 0, :text_color => "425065"}, :position => :center)
    move_down 10
    table([
      ["<a href='http://www.jube.co'><color rgb='67bffd'>www.jube.co</color></a>"],
      ["<a href='mailto:Help@jube.co'><color rgb='67bffd'>Help@jube.co</color></a>"],
      ["This is a receipt sent from JUBE as a service. If you have any inquiries reach out to us."]
      ],:width => 300, :cell_style => {:align => :center, :inline_format => true, :border_width => 0, :size => 7, :padding => 0}, :position => :center)
  end
 
end
