# == Schema Information
#
# Table name: test_card_details
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  number     :string(255)
#  cvv        :string(255)
#  exp_month  :string(255)
#  exp_year   :string(255)
#  person_id  :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class TestCardDetail < ActiveRecord::Base
  attr_accessible :cvv, :exp_year, :exp_month, :name, :number, :person_id
  belongs_to :person

  validates_presence_of :cvv, :exp_year, :exp_month, :name, :number
end
