Rails.application.config.middleware.use OmniAuth::Builder do
  provider :stripe_connect, Rails.configuration.stripe[Rails.env.to_sym][:client_id], Rails.configuration.stripe[Rails.env.to_sym][:secret_key]
end
